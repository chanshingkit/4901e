

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  TextInput,
  ListView,
  ScrollView,
  RecyclerViewBackedScrollView,
  TouchableHighlight,
  DrawerLayoutAndroid
} from 'react-native';

import navigationView from './navigationView';


var Button = require('react-native-button');
import api from './api'
var moment = require('moment');

function prettyDate(date) {
    if(date) {
        return new moment(date).format('YYYY-MM-DD hh:mm');
    }
    return 'N/A';
}

class Events extends Component {
	
	constructor(props) {
		super(props);
		var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.state = { 
			ds: ds
		};
		this._update().done();
	}
	
	async _update() {
		var result = await api.events();
		if(result.status) {
			console.log(result.events);
			this.setState({ds: this.state.ds.cloneWithRows( result.events ), points: result.points, hours_current_year: result.hours_current_year, points_current_scheme_year: result.points_current_scheme_year, rank: result.rank});
		} else {
			alert('cannot load events');
		}
	}

    _renderRecordRow(row) {
        return (
            <View>
			    <Text>
				    <Text style={styles.title}>Join Time: </Text>
				    {'\n\t\t\t\t'+ prettyDate(row.joinTime)}
			    </Text>
			    <Text>
				    <Text style={styles.title}>Leave Time: </Text>
				    {'\n\t\t\t\t' + prettyDate(row.leaveTime)}
			    </Text>
            </View>
        );
    }
	
	_renderRow(row) {
		console.log('rendering',row);
        var self = this;
		return (
			<View style={styles.eventRow}>
				<Text>
					<Text style={styles.title}>Event:  </Text>
					{'\n\t\t\t\t' + ((row.session && row.session.event) ? row.session.event.name_eng : 'Unknown Event')}
				</Text>
				<Text>
					<Text style={styles.title}>Session:</Text>
					{'\n\t\t\t\t' + (row.session ? row.session.name_eng : 'Unknown Session')}
				</Text>
				<Text>
					<Text style={styles.title}>Hours: </Text>
					{'\n\t\t\t\t' +row.hours}
				</Text>
				<Text>
					<Text style={styles.title}>Points: </Text>
					{'\n\t\t\t\t' +row.points} 
				</Text>
				<ListView dataSource={ new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}).cloneWithRows( row.records )}
					    renderRow={self._renderRecordRow}
				/>
			</View>
		);
	}

	render() {
		return (
			    <ScrollView>
			        <View style={styles.eventRow}>
				        <Text>
					        <Text style={styles.title}>Total Points: </Text>
					        {this.state.points}
				        </Text>
				        <Text>
					        <Text style={styles.title}>Points in this scheme year: </Text>
					        {this.state.points_current_scheme_year}
				        </Text>
				        <Text>
					        <Text style={styles.title}>Hours in this year: </Text>
					        {this.state.hours_current_year}
				        </Text>
				        <Text> 
					        <Text style={styles.title}>Rank: </Text>
					        {this.state.rank ? this.state.rank.name_eng : 'Unknown'}
				        </Text>
                    </View>
				    <ListView dataSource={this.state.ds}
					    renderRow={this._renderRow.bind(this)}
				    />
			    </ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	title: {fontSize: 15,
			fontWeight: 'bold',},
    eventRow: { 
        marginLeft: 10, 
        marginRight: 10, 
        marginTop: 10,
        marginBottom: 10,
        paddingLeft: 10, 
        paddingRight: 10, 
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#FFFFFF',
		borderRadius: 15,
    },
});


export default Events

