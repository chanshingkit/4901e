

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableHighlight,
  TouchableNativeFeedback,
  ViewPagerAndroid,
  ScrollView,
  TouchableOpacity,
  NativeModules
} from 'react-native';


import DropDown, {
  Select,
  Option,
  OptionList,
} from 'react-native-selectme';


var Button = require('react-native-button');
var AwesomeButton = require('react-native-awesome-button')
var Dropdown = require('react-native-dropdown-android');
import api from './api'

class SignIn extends Component {
	
	constructor(props) {
		super(props);
		this.state = { 
			email: '',
			pass: '',
            isSignIn: true ,
		};
	}
	
	async _signin() {
		var result = await api.signin(this.state.email, this.state.pass);
		if(result.status) {
			this.props.navigator.replace(this.props.navigator.routes.home);
		} else {
			alert('wrong user or pass');
		}
	}
	
	async _register() {
		var result = await api.register(this.state);
		if(result.status) {
			this.props.navigator.replace(this.props.navigator.routes.home);
		} else {
			alert('cannot register');
		}
	}

    pickDOB() {
        var self = this;
        NativeModules.DateAndroid.showDatepickerWithInitialDate(this.state.dob, function() {
        }, function(year, month, day) {
            self.setState({ dob: new Date(year, month, day).toString() });
        });
    }

    pickEffectiveTo() {
        var self = this;
        NativeModules.DateAndroid.showDatepickerWithInitialDate(this.state.effectiveTo, function() {
        }, function(year, month, day) {
            self.setState({ effectiveTo: new Date(year, month, day).toString() });
        });
    }

	render() {
		return (
            <View style={styles.page}>
                <View style={styles.header}>
                    <TouchableNativeFeedback onPress={()=>this.refs['pager'].setPage(0)}>
                        <View style={[styles.headerItem, this.state.isSignIn && styles.headerItemActive]}>
                            <Text style={styles.headerItemText}>Sign In</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback onPress={()=>this.refs['pager'].setPage(1)}>
                        <View style={[styles.headerItem, !this.state.isSignIn && styles.headerItemActive]}>
                            <Text style={styles.headerItemText}>Register</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
                <ViewPagerAndroid ref="pager" initialPage={0} style={styles.pageStyle} onPageScroll={(e)=>this.setState({isSignIn: e.nativeEvent.position == 0})}>
			        <View style={styles.view}>
			            <ScrollView style={{ flex: 1 }}>
			                <View style={styles.view}>
                                <Image style={styles.icon} source={require('./logo.png')} />
                                 <View style= {{}}>
				                    <TextInput style={styles.textinput}
                                      ref= "email"
				                      keyboardType="email-address"
                                      onChangeText={(text) => this.setState({email: text})}
                                      value={this.state.email}
                                      placeholder="Email"
                                    />
                                 </View>
				                <TextInput style={styles.textinput}
                                  ref= "pass"
				                  secureTextEntry={true}
                                  onChangeText={(text) => this.setState({pass: text})}
                                  value={this.state.pass}
                                  placeholder="Password"
                                />
								
								<View style={styles.container}>
									<AwesomeButton  backgroundStyle={styles.loginButtonBackground}
									labelStyle={styles.loginButtonLabel}
									transitionDuration={200}
									states={{
									  default: {
										text: 'Log In',
										onPress: this._signin.bind(this),
										backgroundColor: '#2196F3',
									  }
									}}
									/>
								</View>
                            </View>
                        </ScrollView>
			        </View>


					<View style={{ flex: 1, alignItems: 'stretch' }}>
						<ScrollView style={{ flex: 1 }}>
						<Text style={styles.headerTitle}>Account Information: </Text>
							<Text style={styles.title}>Email: </Text>
								<TextInput
								  ref= "email"
								  keyboardType="email-address"
								  onChangeText={(text) => this.setState({email: text})}
								  value={this.state.email}
								/>
							<Text style={styles.title}>Password: </Text>
								<TextInput
								  ref= "pass"
								  secureTextEntry={true}
								  onChangeText={(text) => this.setState({pass: text})}
								  value={this.state.pass}
								/>
								<View style={styles.container}>
									<AwesomeButton  backgroundStyle={styles.nextButtonBackground}
									labelStyle={styles.nextButtonLabel}
									transitionDuration={200}
									states={{
									  default: {
										text: 'Next',
										onPress: ()=>this.refs['pager'].setPage(2), 
										backgroundColor: '#2196F3',
									  }
									}}
									/>
								</View>
						</ScrollView>	
					</View>	
					<View style={{ flex: 1, alignItems: 'stretch' }}>
						<ScrollView style={{ flex: 1 }}>
						<Text style={styles.headerTitle}>Basic Information: </Text>
							<Text style={styles.title}>UniYID: </Text>
							<TextInput
								ref= "UniYID"
								onChangeText={(text) => this.setState({UniYID: text})}
								value={this.state.UniYID}
							/>

							<Text style={styles.title}>Effective To: </Text>
							<TouchableOpacity onPress={this.pickEffectiveTo.bind(this)}> 
								<Text style={{fontSize:20}}>      Click here to pick a date</Text>
								<Text>{this.state.effectiveTo}</Text>
							</TouchableOpacity>
							<View style={styles.container}>
									<AwesomeButton  backgroundStyle={styles.nextButtonBackground}
									labelStyle={styles.nextButtonLabel}
									transitionDuration={200}
									states={{
									  default: {
										text: 'Next',
										onPress: ()=>this.refs['pager'].setPage(3), 
										backgroundColor: '#2196F3',
									  }
									}}
									/>
								</View>
							</ScrollView>	
					</View>	
					<View style={{ flex: 1, alignItems: 'stretch' }}>
						<ScrollView style={{ flex: 1 }}>
							<Text style={styles.headerTitle}>Personal Information: </Text>
							<Text style={styles.title}>English Full Name: </Text>
							<TextInput
							  placeholder ="E.g. Chan Tai Man"
							  ref= "name_eng"
							  onChangeText={(text) => this.setState({name_eng: text})}
							  value={this.state.name_eng}
							/>
							<Text style={styles.title}>Chinese Full Name: </Text>
							<TextInput
							  placeholder ="E.g. 陳大文"
							  ref= "name_chi"
							  onChangeText={(text) => this.setState({name_chi: text})}
							  value={this.state.name_chi}
							/>

							<Text style={styles.title}>Gender: </Text>
							<Dropdown 
									style={{ height: 30, width: 250}}
									values={[ 'Select your gender ...', 'Male', 'Female']}
									selected={1} onChange={(data) => this.setState({gender: data.value})} />
							<Text style={styles.title}>Phone No.: </Text>
							<TextInput
							  ref= "phoneNo"
							  onChangeText={(text) => this.setState({phoneNo: text})}
							  value={this.state.phoneNo}
							/>
							<View style={styles.container}>
									<AwesomeButton  backgroundStyle={styles.nextButtonBackground}
									labelStyle={styles.nextButtonLabel}
									transitionDuration={200}
									states={{
									  default: {
										text: 'Next',
										onPress: ()=>this.refs['pager'].setPage(4), 
										backgroundColor: '#2196F3',
									  }
									}}
									/>
							</View>
						</ScrollView>	
					</View>	
					<View style={{ flex: 1, alignItems: 'stretch' }}>
						<ScrollView style={{ flex: 1 }}>
							<Text style={styles.headerTitle}>HKUST Information: </Text>
							<Text style={styles.title}>Student ID: </Text>
							<TextInput
							  ref= "UST.stid"
							  keyboardType="numeric"
							  onChangeText={(text) => this.setState({"UST.stid": text})}
							  value={this.state["UST.stid"]}
							/>

							<Text style={styles.title}>ITSC: </Text>
							<TextInput
							  ref= "UST.itsc"
							  onChangeText={(text) => this.setState({"UST.itsc": text})}
							  value={this.state["UST.itsc"]}
							/>

							<Text style={styles.title}>School: </Text>

							<Dropdown 
									style={{ height: 30, width: 300}}
								    ref="UST.school"
									values={[ 'Select your school ...', 'School of Business', 'School of Science', 'School of Engineering', 'School of Humanities and Social Science']}
									selected={1} onChange={(data) => this.setState({"UST.school": data.value})} />

							<Text style={styles.title}>Primary Language: </Text>
							<TextInput
							  ref= "Launguage"
							  onChangeText={(text) => this.setState({"Launguage": text})}
							  value={this.state["Launguage"]}
							/>
								<View style={styles.container}>
									<AwesomeButton  backgroundStyle={styles.nextButtonBackground}
									labelStyle={styles.nextButtonLabel}
									transitionDuration={200}
									states={{
									  default: {
										text: 'Next',
										onPress: ()=>this.refs['pager'].setPage(5), 
										backgroundColor: '#2196F3',
									  }
									}}
									/>
								</View>
						</ScrollView>	

					</View>	
					<View style={{ flex: 1, alignItems: 'stretch' }}>
						<ScrollView style={{ flex: 1 }}>
							<Text style={styles.headerTitle}>Emergency Contact: </Text>
							<Text style={styles.title}>Name: </Text>
							<TextInput
							  ref= "EmergencyContact.Name"
							  onChangeText={(text) => this.setState({"EmergencyContact.Name": text})}
							  value={this.state["EmergencyContact.Name"]}
							/>

							<Text style={styles.title}>Relationship: </Text>
							<TextInput
							  ref= "EmergencyContact.Relationship"
							  onChangeText={(text) => this.setState({"EmergencyContact.Relationship": text})}
							  value={this.state["EmergencyContact.Relationship"]}
							/>

							<Text style={styles.title}>Telephone: </Text>
							<TextInput
							  ref= "EmergencyContact.Tel"
							  onChangeText={(text) => this.setState({"EmergencyContact.DaytimeTel": text})}
							  value={this.state["EmergencyContact.DaytimeTel"]}
							/>

							<View style={styles.container}>
							 <AwesomeButton  backgroundStyle={styles.registerButtonBackground}
											labelStyle={styles.loginButtonLabel}
											transitionDuration={200}
											states={{
											  default: {
												text: 'Register',
												onPress: this._register.bind(this),
												backgroundColor: '#2196F3',
											  }
											}}
							/>
							</View>	
						</ScrollView>

					</View>
					
					
                </ViewPagerAndroid>
            </View>
		);
	}
}

const styles = StyleSheet.create({
	title: { fontWeight: '500', marginLeft: 10, },
    view: {alignItems: 'center', paddingTop: 20},
    icon: { width: 290, height: 250, alignItems: 'center', },
    pageStyle: { alignItems: 'stretch', flex: 1 },
    page: { alignItems: 'stretch', flex: 1 },
    header: { backgroundColor: '#2196F3', height: 56, flexDirection: 'row' },
    headerItem: { flex: 0.5, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 4, borderBottomColor: '#2196F3' },
    headerItemText: { color: '#fff', fontSize: 16},
    headerItemActive: { borderBottomColor: '#18FFFF' },
    headerTitle: { marginLeft: 5,fontWeight: '900', fontSize: 20 },
    textinput: {paddingLeft: 30 , paddingBottom: 10 , fontSize: 18},
	container: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		margin: 20
	},
	loginButtonBackground: {
		flex: 1,
		height: 40,
		width: 250, 
		borderRadius: 10
	},
	loginButtonLabel: {
		color: 'white'
	},
	registerButtonBackground: {
		flex: 1,
		height: 40, 
		width: 320, 
		borderRadius: 10,
		alignItems: 'center',
		justifyContent: 'center',  
	},
	registerButtonLabel: {
		color: 'white',
		fontSize: 18
	},
	nextButtonBackground: {
		flex: 1,
		height: 40, 
		width: 320, 
		borderRadius: 10, 
		alignItems: 'center',
		justifyContent: 'center', 
	}, 
	nextButtonLabel: {
		color: 'white'
	},
});


export default SignIn

