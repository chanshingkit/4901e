import React, {
  AppRegistry,
  Component,
} from 'react-native';
import BarcodeScanner from 'react-native-barcodescanner';
import api from './api'
import Events from './events'
 
class BarcodeScannerExampleApp extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      torchMode: 'off',
      cameraType: 'back',
    };
  }
 
  barcodeReceived(e) {
    console.log('Barcode: ' + e.data);
    console.log('Type: ' + e.type);
	this._checkin(e.data).done();
  }
  
  async _checkin(code) {
	if(code) {
		var m = code.match(/checkin\/([0-9A-Za-z\-]+)/);
		var m2 = code.match(/checkout\/([0-9A-Za-z\-]+)/);
		console.log(m);
		console.log(m2);
		if(m && m[1]) {
			var result = await api.checkin(m[1]);
			if(result.status) {
				alert('Check In Success');
				this.props.navigator.replace(this.props.navigator.routes.events);
			} else {
				alert('Cannot Checkin: ' + result.msg);
				this.props.navigator.pop();
			}
		} else if(m2 && m2[1]) {
			var result = await api.checkout(m2[1]);
			if(result.status) {
				alert('Check Out Success');
				this.props.navigator.replace(this.props.navigator.routes.events);
			} else {
				alert('Cannot Checkout: ' + result.msg);
				this.props.navigator.pop();
			}
        }
	}
  }
 
  render() {
    return (
      <BarcodeScanner
        onBarCodeRead={this.barcodeReceived.bind(this)}
        style={{ flex: 1 }}
        torchMode={this.state.torchMode}
        cameraType={this.state.cameraType}
      />
    );
  }
}


export default BarcodeScannerExampleApp