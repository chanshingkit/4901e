var mongoose = require('mongoose');
var uuid = require('node-uuid');

module.exports = function (db) {

    var schema = mongoose.Schema({
        name_eng: String,
        name_chi: String,       
        disabled: { type: Boolean, default: false },
        points_perhr: { type: Number, default: 0 }
    });

    schema.methods.basic = function() {
        return {
            name_eng: this.name_eng,
            name_chi: this.name_chi,
            points_perhr: this.points_perhr
        };
    }

    var Event = mongoose.model('EventRank', schema);
    return Event;
};


