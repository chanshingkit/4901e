﻿
var mongoose = require('mongoose');
var moment = require('moment');

module.exports = function (db) {
    
    var schema = mongoose.Schema({
        session: { type: mongoose.Schema.ObjectId, ref: 'EventSession' },
        user: { type: mongoose.Schema.ObjectId, ref: 'User' },
        volunteer_no: String,
        records: [
            {
                joinTime: { type: Date, default: null },
                leaveTime: { type: Date, default: null },
                lat: Number,
                lng: Number,
            }
        ],
        hours: { type: Number, default: 0 },
        eventRank: { type: mongoose.Schema.ObjectId, ref: 'EventRank' },
        points: { type: Number, default: 0 }
    });

    schema.methods.basic = function() {
        return {
            session: this.session ? this.session.basic() : null,
            records: this.records,
            hours: this.hours,
            points: this.points,
            eventRank: this.eventRank ? this.eventRank.basic() : null
        };
    }

    schema.methods.restTime = function() {
        var self = this;
        var joinTime = null, leaveTime = null;

                var duration = 0;
                var startTime = self.session && self.session.startTime ? moment(self.session.startTime) : null;
                var endTime = self.session && self.session.endTime ? moment(self.session.endTime) : null;
                if(startTime && endTime && endTime.valueOf() < startTime.valueOf()) {
                    var cur = startTime;
                    startTime = endTime;
                    endTime = cur;
                }

                for(var i = 0; i < self.records.length; i++) {
                    var record = self.records[i];
                    if(record.joinTime && record.leaveTime) {
                        var a = moment(record.joinTime);
                        var b = moment(record.leaveTime);
                        if(startTime && a.valueOf() < startTime.valueOf()) a = startTime;
                        if(endTime && b.valueOf() > endTime.valueOf()) b = endTime;
                        var diff = b.diff(a);
                        if(diff > 0)
                            duration += diff;
                        if(!joinTime) joinTime = a;
                        leaveTime = b;
                    }
                }

        if(joinTime && leaveTime) {
            return moment.duration(Math.max(0, leaveTime.diff(joinTime) - duration)).asMinutes();
        }
    }

    var EventRecord = mongoose.model('EventRecord', schema);

    schema.pre('save', function(next) {
        this.populate("session eventRank", function(err, self) {
            if(err) next(err);
            else {
                var duration = 0;
                var startTime = self.session.startTime ? moment(self.session.startTime) : null;
                var endTime = self.session.endTime ? moment(self.session.endTime) : null;
                if(startTime && endTime && endTime.valueOf() < startTime.valueOf()) {
                    var cur = startTime;
                    startTime = endTime;
                    endTime = cur;
                }

                for(var i = 0; i < self.records.length; i++) {
                    var record = self.records[i];
                    if(record.joinTime && record.leaveTime) {
                        var a = moment(record.joinTime);
                        var b = moment(record.leaveTime);
                        if(startTime && a.valueOf() < startTime.valueOf()) a = startTime;
                        if(endTime && b.valueOf() > endTime.valueOf()) b = endTime;
                        var diff = b.diff(a);
                        if(diff > 0)
                            duration += diff;
                    }
                }

                var hours = moment.duration(duration).asHours();
                self.hours = Math.round(hours * 2) / 2;
                if ((record) && (record.session) && (record.session.maxHours))
                    self.hours = Math.min(record.session.maxHours, self.hours);
                
                self.points = (self.eventRank ? self.eventRank.points_perhr : 3) * self.hours;
                if ((record) && (record.session) && (record.session.maxPoints))
                    self.points = Math.min(record.session.maxPoints, self.points);

                next();
            }
        });
    });

    return EventRecord;
};


