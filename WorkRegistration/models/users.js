﻿
var crypto = require('crypto'),
    config = require('../config');
var mongoose = require('mongoose');
var generatePassword = require("password-generator");
var uuid = require('node-uuid');

module.exports = function (db) {
    //Up to down, left to right
    //Prog_Application form 20151017
    
    var schema = mongoose.Schema({
        name_eng: String,
        name_chi: String,
        name_nick: String,
        gender: String,
        phoneNo: { type: String },
        email: { type: String, index: { unique: true }, sparse: true },
        UST: {
            stid: { type: String, index: { unique: true }, sparse: true },
            itsc: { type: String, index: { unique: true }, sparse: true },
            school: String,
            yearOfStudy: { type: Number, default: 0 },
        },
        Launguage: String,
        EmergencyContact: {
            Name: String,
            Relationship: String, 
            Tel: String
        },
        
        UniYID: { type: String, index: { unique: true }, sparse: true },
        effectiveTo: { type: Date, default: Date.now },
        password: String,
        token: { type: String, index: { unique: true }, sparse: true },
        date: { type: Date, default: Date.now },
        lastDate: { type: Date, default: Date.now },
        lastIP: String,
        disabled: { type: Boolean, default: false },
        points: { type: Number, default: 0 }, //only count to last year
        rank: { type: mongoose.Schema.ObjectId, ref: 'Rank' },
        verified: { type: Boolean, default: false },
    });
    
    schema.statics.hash = function (password) {
        return crypto.createHmac('sha1', config.hashSecret).update(password + config.passwordSecret).digest('hex');
    };
    
    schema.statics.loginUser = function (email, password, cb) {
        this.findOne({ email: email }, function (err, user) {
            if (user) {
                if (user.password != User.hash(password)) {
                    cb('bad password');
                } else {
                    cb(null, user);
                }
            } else {
                cb('no such user', null);
            }
        });
    };
    
    schema.statics.validateUser = function (token, cb) {
        this.findOne({ token: token }, function (err, user) {
            if (user) {
                cb(user);
            } else {
                cb(null);
            }
        });
    };
    
    schema.statics.token = function () {
        return uuid.v4();
    }

    schema.methods.basic = function() {
        return {
            name_eng: this.name_eng,
            name_chi: this.name_chi,
            itsc: this.UST.itsc
        };
    }
    
    var User = mongoose.model('User', schema);
    return User;
};


