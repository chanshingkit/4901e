//Sorry I'm too lazy to build more dumb table
//Actually I don't have enough time...

var mongoose = require('mongoose');
var config = require('../config');
var autoIncrement = require("mongoose-auto-increment");

var model = module.exports;
model.init = function (cb) {
    mongoose.connect(config.db);
    
    var db = mongoose.connection;
    db.on('error', function (err) {
        console.log('connection error', err);
    });
    db.once('open', function () {
        console.log('connected to db.');
        //cb();
        dowork(function (err) {
            cb(err);
        });
    });
    
    autoIncrement.initialize(db);
    
    model.db = db;
    
    model.users = require('../models/users.js')(db);
    model.events = require('../models/events.js')(db);
    model.eventRecords = require('../models/eventRecords.js')(db);
    model.ranks = require('../models/ranks.js')(db);
    model.eventRanks = require('../models/eventRanks.js')(db);
    model.staffs = require('../models/staffs.js')(db);
}

var erank = [
    {
        name_eng: "general",
        name_chi: "常務性協助",
        disabled: false,
        points_perhr: 1
    },
    {
        name_eng: "technical",
        name_chi: "技術性協助",
        disabled: false,
        points_perhr: 2
    },
    {
        name_eng: "managerial",
        name_chi: "統籌或帶領",
        disabled: false,
        points_perhr: 3
    }
];

var dowork = function (cb) {
    add_erank(0, function (err) {
        if (err) {
            return cb(err);
        } else {
            var staff = new model.staffs({ username: 'root', StaffID: 'root', StaffName: 'root' });
            staff.setPassword("root");
            staff.save(function(err) {
                return cb(err);
            });
        }
    });
}

var add_erank = function (cur, cb) {
    if (cur == erank.length) {
        return cb();
    } else {
        var nobj = new model.eventRanks(erank[cur]);
        nobj.save(function (err, doc) {
            if (err) {
                return cb(err);
            } else {
                //console.log(erank[cur].name_eng);
                add_erank(cur + 1, cb);
            }
        });
    }
}

//Main
model.init(function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log("done");
        process.exit(0);
    }
});