﻿
var models = require('../models');
var xlsx = require('node-xlsx');
var async = require('async');
var util = require('util');


function parseDate(text) {
    if(!text) return null;
    var cur = parseFloat(text);
    if(text.length != 8 && !Number.isNaN(cur)) {
        return new Date(0, 0, cur-2);
    }
    text = text.substring(0, 4) + "/" + text.substring(4, 6) + "/" + text.substring(6, 8);
    return new Date(text);
}

function parseDateOrTime(date, time) {
    if(!time) return null;
    var cur = parseFloat(time);
    if(time.length != 4 && !Number.isNaN(cur)) {
        if(!date) throw new Error("Bad date ref");
        var pdate = parseDate(date);
        return new Date(pdate.getFullYear(), pdate.getMonth(), pdate.getDate(), Math.floor(cur * 24), cur * 24 % 1 * 60);
    } else {
        if(time.length > 8) return new Date(time);
        if(!date) throw new Error("Bad date ref");
        var pdate = parseDate(date);
        if(time.length == 3) time = "0" + time;
        if(time.length == 4) {
            time = time.substring(0, 2) + ":" + time.substring(2, 4) + ":00";
        }
        return new Date(pdate.getFullYear() + "/" + (pdate.getMonth() + 1) + "/" + (pdate.getDate() + 1) + " " + time);
    }
}

function parseNumber(text) {
    if(!text) return null;
    var num = parseInt(text);
    if(Number.isNaN(num)) throw "Bad number format";
    return num;
}

function parseDecimal(text) {
    if(!text) return null;
    var num = parseFloat(text);
    if(Number.isNaN(num)) throw "Bad number format";
    return num;
}

function parseString(text) {
    return text ? text.toString() : text;    
}

//Student ID	
//Name in English
//Name in Chinese
//Nick Name
//Mobile Tel No.
//ITSC
//Preferable email
//Gender
//School
//Year_of_study
//Uni-Y Membership No
//Effective Date To
//Emergency Contact
//Relationship
//Emer Tel No
//Program_code
//Receipt No
//Amount ($)
//Receipt Date
//Tee size
//Volunteer-profile

var mappings1 = [
    {
        name: "Student ID",
        dbName: "UST.stid",
        dbType: 'users',
    },
    {
        name: "Name in English",
        dbName: "name_eng",
        dbType: 'users',
    },
    {
        name: "Name in Chinese",
        dbName: "name_chi",
        dbType: 'users',
    },
    {
        name: "Nick Name",
        dbName: "name_nick",
        dbType: 'users',
    },
        {
        name: "Mobile Tel No.",
        dbName: "phoneNo",
        dbType: 'users',
    },
    {
        name: "ITSC",
        dbName: "UST.itsc",
        dbType: 'users',
    },
    {
        name: "Preferable email",
        dbName: "email",
        dbType: 'users',
    },
    {
        name: "Gender",
        dbName: "gender",
        dbType: 'users',
    },
    {
        name: "School",
        dbName: "UST.school",
        dbType: 'users',
    },
    {
        name: "Year_of_study",
        dbName: "UST.yearOfStudy",
        dbType: 'users',
        process: parseNumber
    },
    {
        name: "Uni-Y membership no",
        dbName: "UniYID",
        dbType: 'users',
    },
    {
        name: "Effective Date To",
        dbName: "effectiveTo",
        dbType: 'users',
        process: parseDate
    },
    {
        name: "Emergency Contact",
        dbName: "EmergencyContact.Name",
        dbType: 'users',
    },
    {
        name: "Relationship",
        dbName: "EmergencyContact.Relationship",
        dbType: 'users',
    },
    {
        name: "Emer Tel No",
        dbName: "EmergencyContact.Tel",
        dbType: 'users',
    },

    {
        name: "Student ID",
        dbName: "UST.stid",
        dbType: 'eventRegs',
    },
    {
        name: "Program_code",
        dbName: "EventID",
        dbType: 'eventRegs',
    },
    {
        name: "Receipt No",
        dbName: "no",
        dbType: 'eventRegs',
    },
    {
        name: "Amount ($)",
        dbName: "amount",
        dbType: 'eventRegs',
        process: parseDecimal,
    },
    {
        name: "Receipt Date",
        dbName: "date",
        dbType: 'eventRegs',
    },
    {
        name: "Tee size",
        dbName: "tee_size",
        dbType: 'eventRegs',
    },
];

//Student_no	Program_code	Volunteer_no	Session_no	sign_in	sign_out	Serv_hr_earned	Score_earned

var mappings2 = [
    {
        name: "Student_no",
        dbName: "UST.stid",
        dbType: 'eventRecords',
    },
    {
        name: "Program_code",
        dbName: "EventID",
        dbType: 'eventRecords',
    },
    {
        name: "Volunteer_no",
        dbName: "volunteer_no",
        dbType: 'eventRecords',
    },
    {
        name: "Session_no",
        dbName: "SessionID",
        dbType: 'eventRecords',
    },
    {
        name: "sign_in",
        dbName: "joinTime",
        dbType: 'eventRecords',
    },
    {
        name: "sign_out",
        dbName: "leaveTime",
        dbType: 'eventRecords',
    },
    {
        name: "Serv_hr_earned",
        dbName: "hours",
        dbType: 'eventRecords',
        process: parseDecimal
    },
    {
        name: "Score_earned",
        dbName: "points",
        dbType: 'eventRecords',
        process: parseDecimal
    }
];

//program_code	prog_eng_name	prog_chi_name	session_no	location	session_name	session_start_date	session_end_date	session_start_time	session_end_time	Serv_hr_entitled	Score_entitled

var mappings3 = [
    {
        name: "Program_code",
        dbName: "EventID",
        dbType: 'events',
    },
    {
        name: "prog_eng_name",
        dbName: "name_eng",
        dbType: 'events',
    },
    {
        name: "prog_chi_name",
        dbName: "name_chi",
        dbType: 'events',
    },

    {
        name: "Program_code",
        dbName: "EventID",
        dbType: 'eventSessions',
    },
    {
        name: "session_no",
        dbName: "SessionID",
        dbType: 'eventSessions',
    },
    {
        name: "location",
        dbName: "location",
        dbType: 'eventSessions',
    },
    {
        name: "session_name",
        dbName: "name_chi",
        dbType: 'eventSessions',
    },
    {
        name: "session_name",
        dbName: "name_eng",
        dbType: 'eventSessions',
    },
    {
        name: ["session_start_date", "session_start_time"],
        dbName: "startTime",
        dbType: 'eventSessions',
        process: parseDateOrTime
    },
    {
        name: ["session_end_date", "session_end_time"],
        dbName: "endTime",
        dbType: 'eventSessions',
        process: parseDateOrTime
    },
    {
        name: "Serv_hr_entitled",
        dbName: "maxHours",
        dbType: 'eventSessions',
        process: parseDecimal
    },
    {
        name: "Score_entitled",
        dbName: "maxPoints",
        dbType: 'eventSessions',
        process: parseDecimal
    }
];

var mappings = 
[
    {
        name: 'Volunteer-profile',
        target: 'users',
        data: mappings1,
    },
    {
        name: 'Volunteer_attendency',
        target: 'records',
        data: mappings2,
    },
    {
        name: 'Program_session',
        target: 'events',
        data: mappings3,
    },
];

var builds = [
    {
        name: 'users',
        dbType: 'users',
        key: ['UST.stid'],
        dbKey: ['UST.stid'],
        transform: function(item, cb) {
            item.verified = true;
            item.password = models.users.hash(item.phoneNo);
            cb();
        }
    },
    {
        name: 'events',
        dbType: 'events',
        key: ['EventID'],
        dbKey: ['EventID'],
    },
    {
        name: 'users',
        dbType: 'eventRegs',
        key: ['UST.stid', 'EventID'],
        dbKey: ['user', 'event'],
        from: [
            { dbType: 'users', name: ['UST.stid'], target: 'user' },
            { dbType: 'events', name: ['EventID'], target: 'event' }
        ]
    },
    {
        name: 'events',
        dbType: 'eventSessions',
        key: ['EventID', 'SessionID'],
        dbKey: ['SessionID', 'event'],
        from: [
            { dbType: 'events', name: ['EventID'], target: 'event' }
        ],
        transform: function(item, cb) {
            models.eventRanks.findOne({ points_perhr: 3 }, function(err, rank) {
                if(err) cb(err);
                else {
                    item.eventRank = rank._id;
                    cb();
                }
            });
        }
    },
    {
        name: 'records',
        dbType: 'eventRecords',
        key: ['EventID', 'UST.stid', 'SessionID'],
        dbKey: ['user', 'session'],
        from: [
            { dbType: 'users', name: ['UST.stid'], target: 'user' },
            { dbType: 'eventSessions', name: ['EventID', 'SessionID'], target: 'session' },
        ],
        transform: function(item, cb) {
            if(item.joinTime && item.leaveTime) {
                item.records = [
                    {
                        joinTime: item.joinTime,
                        leaveTime: item.leaveTime
                    }
                ]
            }
            if(!item.points && item.hours) item.points = item.hours * 3;
            if(item._session.maxPoints && item._session.maxPoints > item.points) item.points = item._session.maxPoints;
            cb();
        }
    },
]

function findItem(items, func) {
    for(var i = 0; i < items.length; i++)
        if(func(items[i])) return items[i];
    return null;
}

function findItemIdx(items, func) {
    for(var i = 0; i < items.length; i++)
        if(func(items[i])) return i;
    return -1;
}

function compareItem(dbItem, item, key) {
    var args = key.split('.');
    var cur = dbItem;
    for(var i = 0; i < args.length && cur; i++)
        cur = cur[args[i]];
    if(cur) {
        return JSON.stringify(cur) === JSON.stringify(item[key]);
    }
    return -1;
}

function hasSchema(dbItem, key) {
    var args = key.split('.');
    var cur = dbItem;
    for(var i = 0; i < args.length && cur; i++)
        cur = cur[args[i]];
    return !!cur;
}

function logger() {
    this.logs = [];
    this.isError = false;
}


logger.prototype.info = function(msg) {
    this.logs.push({ type: 'info', msg: msg});
}

logger.prototype.error = function(msg) {
    this.isError = true;
    this.logs.push({ type: 'error', msg: msg});
}

logger.prototype.warn = function(msg) {
    this.logs.push({ type: 'warn', msg: msg});
}

exports.doit = function(excelData, save, cb) {
    //var save = true;
    var obj = xlsx.parse(excelData);

    var log = new logger();
    var datas = {};
    for(var i = 0; i < mappings.length; i++) {
        var mapping = mappings[i];
        var dataList = [];
        datas[mapping.target] = dataList;

        var sheet = findItem(obj, function(x) { return x.name == mapping.name; });
        if(!sheet) {
            log.error("Cannot find sheet " + mapping.name);
            continue;
        }

        var mappingIdx = new Array(mapping.data.length);
        var header = sheet.data[0];

        if(!header) {
            log.error(mapping.name + " does not have a valid header");
            continue;
        }

        for(var j = 0; j < mapping.data.length; j++) {
            var op = mapping.data[j];
            var name = op.name instanceof Array ? op.name : [op.name];
            mappingIdx[j] = name.map(function(n) {
                var idx = findItemIdx(header, function(x) { return x.toLocaleLowerCase() == n.toLocaleLowerCase(); });
                if(idx == -1) log.warn("Cannot find column " + n);
                return idx;
            });
        }

        for(var j = 1; j < sheet.data.length; j++) {
            var row = sheet.data[j];
            var item = {};

            for(var k = 0; k < mapping.data.length; k++) {
                var op = mapping.data[k];
                if(!item[op.dbType]) item[op.dbType] = {};
                var param = mappingIdx[k].map(function(idx) { return row[idx] ? row[idx].toString() : row[idx]; } );
                var val = op.process ? op.process.apply(null, param) : param;
                if(val instanceof Array) val = val[0];
                item[op.dbType][op.dbName] = val;
                if(val) item.hasData = true;
            }
            dataList.push(item);
        }
    }

    var outputs = [];
    var outputDict = {};
    var outputDictDict = {};

    for(var i = 0; i < builds.length; i++) {
        var build = builds[i];
        var source = datas[build.name];
        var dict = {};
        var toAddDict = {};
        
        for(var j = 0; j < source.length; j++) {
            var item = source[j];
            if(item.hasData) {
                var record = item[build.dbType];
                var key = build.key.map(function(k) { return record[k]; });
                var jkey = JSON.stringify(key);
                if(!dict[jkey]) dict[jkey] = [];
                dict[jkey].push(record);
            }
        }

        var keys = Object.keys(dict);
        var toAddList = [];
        for(var j = 0; j < keys.length; j++) {
            var list = dict[keys[j]];
            if(list.length > 1) {
                var conflicts = [];
                var merged = {};
                for(var k = 0; k < list.length; k++) {
                    var item = list[k];
                    var props = Object.keys(item);
                    var ccols = [];
                    for(var l = 0; l < props.length; l++) {
                        if(merged[props[l]]) {
                            if(merged[props[l]] != item[props[l]]) {
                                ccols.push(props[l]);
                            }
                        } else {
                            merged[props[l]] = item[props[l]];
                        }
                    }
                    if(ccols.length)
                        conflicts.push({ cols: ccols, idx: k });
                }
                if(conflicts.length)
                    log.error("Conflicts in " + conflicts.map(function(conflict) { return "Row " + (conflict.idx + 2) + ": " + conflict.cols.join(','); }).join(";"));
                toAddList.push(merged);
                toAddDict[keys[j]] = merged;
            } else {
                toAddList.push(list[0]);
                toAddDict[keys[j]] = list[0];
            }
        }

        if(build.from) {
            for(var j = 0; j < build.from.length; j++) {
                var fromItem = build.from[j];
                var targetDict = outputDictDict[fromItem.dbType];
                for(var k = 0; k < toAddList.length; k++) {
                    var curItem = toAddList[k];
                    var targetKey = fromItem.name.map(function(x) { return curItem[x]; });
                    var targetJKey = JSON.stringify(targetKey);
                    var targetItem = targetDict[targetJKey];
                    if(!targetItem) {
                        log.error("Cannot find item " + targetKey.join(',') + " from " + fromItem.name.join(','));
                    }
                    curItem[fromItem.target] = targetItem;
                }
            }
        }

        outputs.push(toAddList);
        outputDict[build.dbType] = toAddList;
        outputDictDict[build.dbType] = toAddDict;
    }

    if(log.isError) {
        console.log(log.logs);
        console.log(util.inspect(datas, false, null));
        cb(null, false, log.logs); 
        return;   
    }
    
    var outputStats = {};

    async.eachSeries(builds, function(build, cb) {
        var outputList = outputDict[build.dbType];
        var curStat = outputStats[build.dbType] = { imported: outputList.length, newItems: 0, updated: 0, skipped: 0 };

        async.eachSeries(outputList, function(entry, cb) {

                if(build.from) {
                    for(var i = 0; i < build.from.length; i++) {
                        var fromItem = build.from[i];
                        if(entry[fromItem.target]) {
                            entry["_" + fromItem.target] = entry[fromItem.target];
                            entry[fromItem.target] = entry[fromItem.target]._id;
                        }
                    }
                }

            var cond = {};
            var condValid = true;
            for(var i = 0; i < build.dbKey.length; i++) {
                if(!(cond[build.dbKey[i]] = entry[build.dbKey[i]])) condValid = false;
            }


            models[build.dbType].findOne(cond, function(err, dbItem) {

                if(err) {
                    cb(err);
                    return;
                }

                if(build.transform)
                    build.transform(entry, saveCore)
                else
                    saveCore();

                function saveCore(err) {
                    if(err) {
                        cb(err);
                        return;
                    }
                    var updateItem = {};
                    if(dbItem && condValid) {
                        var entryKeys = Object.keys(entry);
                        var isChange = false;
                        for(var i = 0; i < entryKeys.length; i++) {
                            if(!entry[entryKeys[i]]) continue;
                            var st = compareItem(dbItem, entry, entryKeys[i]);
                            if(st == -1 && hasSchema(models[build.dbType].schema.tree, entryKeys[i]) || !st) {
                                if(!st)
                                    log.warn("Item " + JSON.stringify(dbItem) + " mismatch");
                                updateItem[entryKeys[i]] = entry[entryKeys[i]];
                                isChange = true;
                            }
                        }
                        updateItem._id = dbItem._id;
                        if(isChange)
                            curStat.updated++;
                        else
                            curStat.skipped++;
                    } else {
                        updateItem = entry;
                        curStat.newItems++;

                        updateItem.checkInToken = models.eventSessions.token();
                        updateItem.checkOutToken = models.eventSessions.token();

                    }

                    if(save) {
                        models[build.dbType].findOneAndUpdate(cond, updateItem, { upsert:true, new: true} ,function(err, dbItem) {
                            if(err) {
                                cb(err);
                                return;
                            }

                            entry._id = dbItem._id;
                            cb();
                        });
                    } else {
                            if(dbItem)
                                entry._id = dbItem._id;
                            else
                                entry._id = null;
                            cb();
                    }
                }
            });
        }, function(err) {
            cb(err);
        });
    }, function(err) {

        if(err) cb(err);
        else {

            var excel = xlsx.build([
                {
                    name: 'Volunteer-profile',
                    data: outputDict.eventRegs.map(function(item) {
                        return [
                            item._user['UST.stid'],
                            item._user['name_eng'],
                            item._user['name_chi'],
                            item._user['name_nick'],
                            item._user['phoneNo'],
                            item._user['UST.itsc'],
                            item._user['email'],
                            item._user['gender'],
                            item._user['UST.school'],
                            item._user['UST.yearOfStudy'],
                            item._user['UST.effectiveTo'],
                            item._user['EmergencyContact.Name'],
                            item._user['EmergencyContact.Relationship'],
                            item._user['EmergencyContact.Tel'],
                            item._event['EventID'],
                            item['no'],
                            item['amount'],
                            item['date'],
                            item['tee_size'],
                        ];
                    })
                },
                {
                    name: 'Volunteer_attendency',
                    data: outputDict.eventRecords.map(function(item) {
                        return [
                            item._user['UST.stid'],
                            item._session._event['EventID'],
                            item._session['SessionID'],
                            item['volunteer_no'],
                            item['joinTime'],
                            item['leaveTime'],
                            item['hours'],
                            item['points'],
                        ];
                    })
                },
                {
                    name: 'Program_session',
                    data: outputDict.eventSessions.map(function(item) {
                        return [
                            item._event['EventID'],
                            item._event['name_eng'],
                            item._event['name_chi'],
                            item['SessionID'],
                            item['location'],
                            item['name_eng'],
                            item['startTime'],
                            item['endTime'],
                            item['startTime'],
                            item['endTime'],
                            item['maxHours'],
                            item['maxPoints'],
                        ];
                    })
                },

            ]);

            function infoStat(stat) {
                return "Imported " + stat.imported + " including " + stat.newItems + " new items and " + stat.updated + " updated items " + stat.skipped + " skipped items from ";
            }

            log.info(infoStat(outputStats.users) + " users");
            log.info(infoStat(outputStats.eventRegs) + " user regs");
            log.info(infoStat(outputStats.events) + " events");
            log.info(infoStat(outputStats.eventSessions) + " event sessions");
            log.info(infoStat(outputStats.eventRecords) + " event records");

            console.log(log.logs);
            
            //In: excelData, save, cb 
            //Out: ErrorFromDB, Success, Log, File
            cb(null, true, log.logs, excel);
        }

    });
}

