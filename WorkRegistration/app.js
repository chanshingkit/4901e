﻿var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//var engines = require('consolidate');
var ejs = require('ejs');                       //HTML render
// User authentication (web side only)
var passport = require('passport');
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);

var routes = require('./routes/index');
var users = require('./routes/users');

var config = require('./config');
var user_auth = require('./user_auth.js'); //User Authentication
//var mongo_express = require('mongo-express/lib/middleware');

// Setting up Login Sessions
passport.serializeUser(user_auth.LOGIN_SERIALIZE);
passport.deserializeUser(user_auth.LOGIN_DESERIALIZE);
//passport.use(user_auth.LOGIN_STRATEGY_LOCAL());
passport.use(user_auth.LOGIN_STRATEGY_DB());

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
//app.engine('handlebars', engines.handlebars);
app.engine('html', ejs.renderFile);
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: config.sessionSecret,
    cookie: { },
    resave: false,
    saveUninitialized: true,
    store: new MongoDBStore({ uri: config.db, collection: 'sessions' })
}))

app.use('/', routes);
app.use('/upgrade', require('./routes/upgrade.js'));
app.use('/users', users);
app.use('/exports', require('./routes/exportExcel.js'));
app.use('/api', require('./routes/api.js'));
app.use('/api_darren', require('./routes/api_darren.js'));
//app.use('/mongo_express', mongo_express(config.mongo_express));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
