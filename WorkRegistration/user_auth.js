/**
 *  Login session, logic, mechanism etc. goes here.
 *  Author: Lau Tsz Hei Darren
 **/

// Login
var LocalStrategy = require('passport-local');
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);

var models = require('./models.js');

// Set up 1st stage of configurations
var exports = module.exports = {
    
    // Core logic of Login function
    FINDUSER_LOCAL: function (username, cb) {
        if (username == "user") {
            return cb(null, { username: "user", password: "1234" });
        }
        else if (username == "admin") {
            return cb(null, { username: "admin", password: "55555" });
        }
        else {
            return cb(null, null);
        }
    },
    
    FINDUSER_DB: function (username, password, done) {
        models.staffs.findOne({ username: username }, function (err, user) {
            if (err) { return done(err); }
            else if (!user) { return done(null, false, { message: 'Incorrect username.' }); }
            else if (!user.validPassword(password)) { return done(null, false, { message: 'Incorrect password.' }); }
            else { return done(null, user); }
        });
    },
    
    LOGIN_SESSION: {
        secret: '#Welcome_To_Web_Server_@_UniY!#',
        cookie: { maxAge: 60 * 60 * 1000 }, //1 hour
        resave: true,
        saveUninitialized: true
    }, 
    
    LOGIN_SERIALIZE: function (user, done) {
        //logger.log("debug", "serializing " + user.username);
        done(null, user);
    },
    
    LOGIN_DESERIALIZE: function (obj, done) {
        //logger.log("debug", "deserializing " + obj.username);
        done(null, obj);
    }

};

// Set up 2nd stage of configurations. Some of them are based from above.

exports.LOGIN_STRATEGY_LOCAL = function () {
    return new LocalStrategy(
        function (username, password, done) {
            // asynchronous verification, for effect...
            process.nextTick(function () {
                exports.FINDUSER_LOCAL(username, function (err, user) {
                    //logger.log("debug", user);
                    if (err) { return done(err); }
                    if (!user) { return done(null, false, { message: 'Unknown user ' + username }); }
                    if (user.password != password) { return done(null, false, { message: 'Invalid password' }); }
                    return done(null, user);
                });
            });
        }
    );
},

exports.LOGIN_STRATEGY_DB = function () {
    return new LocalStrategy(
        function (username, password, done) {
            //process.nextTick(function () {
                exports.FINDUSER_DB(username, password, done);
            //});
        }
    )
}

exports.LOGIN_SESSION_DB = {
    secret: exports.LOGIN_SESSION.secret,
    cookie: exports.LOGIN_SESSION.cookie, //1 hour
    resave: false,
    saveUninitialized: true,
    store: new MongoDBStore({ uri: exports.DB_URI, collection: 'sessions' })
};
