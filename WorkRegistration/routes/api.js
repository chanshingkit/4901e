﻿var express = require('express');
var router = express.Router();
var models = require('../models.js');
var moment = require('moment');
var async = require('async');

router.use(function (req, res, next) {
    if (req.body && req.body.token) {
        models.users.validateUser(req.body.token, function (result) {
            if (result) {
                req.user = result
                next();
            } else {
                next();
            }
        });
    } else {
        next();
    }
});

router.post('/checkToken', function (req, res) {
    res.send({ status: !!req.user, user: req.user ? req.user.basic() : null });
});

router.post('/check', function (req, res) {
    models.users.findOne({ email: req.body.email }, function (err, user) {
        if (err || user) {
            res.send({ status: false });
        } else {
            res.send({ status: true });
        }
    });
});

router.post('/profile', function (req, res) {
    delete req.user.password;
    delete req.user.token;
    res.send({ status: true, user: req.user });
});

router.post('/register', function (req, res) {
    var user = new models.users(req.body);
    user.password = models.users.hash(req.body.pass || "");
    user.token = models.users.token();
    user.save(function (err) {
        if (err) {
            res.send({ status: false });
        } else {
            res.send({ status: true, token: user.token, user: user.basic() });
        }
    });
});

router.post('/signin', function (req, res) {
    models.users.loginUser(req.body.email, req.body.pass, function (err, user) {
        if (err) {
            res.send({ status: false });
        } else {
            user.token = models.users.token();
            user.save(function (err) {
                if (err) {
                    res.send({ status: false });
                } else {
                    res.send({ status: true, token: user.token, user: user.basic() });
                }
            });
        }
    });
});

router.post('/events', function (req, res) {
    if (!req.user) {
        res.send({ status: false });
        return;
    }

    models.eventRecords.find({ user: req.user._id }).populate("session eventRank").exec(function (err, events) {
        if (err) {
            res.send({ status: false });
        } else {
            models.events.populate(events, { path: 'session.event' }, function(err, output) {
                if(err) next(err);
                else {
                    var points = 0;
                    var hours = 0;
                    var now = new moment();
                    var currentYear = new moment({years: now.year(), months: 3, date: 1});
                    var nextYear = new moment({years: now.year()+1, months: 3, date: 1});
                    var endTime = now.valueOf() < currentYear.valueOf() ? currentYear : nextYear;
                    var startTime = new moment({years: endTime.year()-1, months: 3, date: 1});
                    for(var i = 0; i < events.length; i++) {
                        var item = events[i];
                        var time = item.session && item.session.startTime ? new moment(item.session.startTime) : null;
                        if(!time) time = item.records[0] && item.records[0].joinTime ? new moment(item.records[0].joinTime) : null;
                        if(time && time.valueOf() >= startTime.valueOf() && time.valueOf() < endTime.valueOf()) {
                            points += item.points;
                        }
                        if(time.year() == now.year())
                            hours += item.hours;
                    }
                    res.send({ status: true, points: req.user.points + points, points_current_scheme_year: points, hours_current_year: hours, rank: req.user.rank ? req.user.rank.basic() : null, events: events.map(function(item) { return item.basic(); }) });
                 }
            });
            }
    });
});

router.post('/changePassword', function (req, res) {
    if (req.user.password == models.users.hash(req.body.pass)) {
        req.user.password = models.users.hash(req.body.new);
        req.user.save(function (err) {
            if (err) {
                res.send({ status: false });
            } else {
                res.send({ status: true });
            }
        });
    } else {
        res.send({ status: false });
    }
});

router.post('/checkin', function (req, res) {
    if (!req.user) {
        res.send({ status: false });
        return;
    }
    
    models.eventSessions.findOne({ checkInToken: req.body.event }, function (err, session) {
        if (err) {
            res.send({ status: false, msg: 'Internal Error' });
        } else if(!session) {
            res.send({ status: false, msg: 'Session not found' });
        } else {
            models.eventRecords.findOne({ session: session._id, user: req.user._id }).exec(function (err, eventRecord) {
                if (err) {
                    res.send({ status: false });
                } else {
                    if (eventRecord) {
                        if(eventRecord.records.length == 0 || eventRecord.records[eventRecord.records.length - 1].leaveTime) {
                            eventRecord.records.push({joinTime: Date.now()});
                            eventRecord.save(function (err) {
                                if (err) {
                                    res.send({ status: false });
                                } else {
                                    res.send({ status: true, msg: "Checkin success" });
                                }
                            });
                        } else {
                            res.send({ status: false, msg: "You cannot checkin twice" });
                        }
                    } else {
                        eventRecord = new models.eventRecords({
                            user: req.user._id,
                            session: session._id,
                            records: [
                                { joinTime: Date.now() }
                            ]
                        });
                        eventRecord.save(function (err) {
                            if (err) {
                                res.send({ status: false });
                            } else {
                                res.send({ status: true, msg: "Checkin success" });
                            }
                        });
                    }
                }
            });
        }
    });
});

router.post('/checkout', function (req, res) {
    if (!req.user) {
        res.send({ status: false });
        return;
    }
    
    models.eventSessions.findOne({ checkOutToken: req.body.event }, function (err, session) {
        if (err) {
            res.send({ status: false, msg: 'Internal Error' });
        } else if(!session) {
            res.send({ status: false, msg: 'Session not found' });
        } else {
            models.eventRecords.findOne({ session: session._id, user: req.user._id }).exec(function (err, eventRecord) {
                if (err) {
                    res.send({ status: false });
                } else {
                    if (eventRecord && eventRecord.records.length) {
                        if(eventRecord.records[eventRecord.records.length - 1].leaveTime) {
                            res.send({ status: false, msg: "You cannot checkout twice" });
                        }
                        else {
                            eventRecord.records[eventRecord.records.length - 1].leaveTime = Date.now();
                            eventRecord.save(function (err) {
                                if (err) {
                                    res.send({ status: false });
                                } else {
                                    res.send({ status: true, msg: "Checkout success" });
                                }
                            });
                        }
                    } else {
                        res.send({ status: false, msg: "You did not checkin" });
                    }
                }
            });
        }
    });
});

module.exports = router;