﻿var async = require('async');
var models = require('../models.js');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res, cb) {

    models.ranks.find({}, function(err, ranks) {
        
        if(err) {
            cb(err);
            return;    
        }

        var rankDict = {};
        for(var i = 0; i < ranks.length; i++)
            rankDict[ranks[i]._id] = ranks[i];

        for(var i = 0; i < ranks.length; i++)
            if(ranks[i].prereq)
                rankDict[ranks[i]._id].next = ranks[i];

        models.users.find({}, function(users, cb) {

            if(err) {
                cb(err);
                return;    
            }

            async.eachSeries(users, function(user, cb) {

                models.eventRecords.find({ user: user._id }).populate("session eventRank").exec(function (err, events) {
                    if (err) {
                        res.send({ status: false });
                    } else {
                        models.events.populate(events, { path: 'session.event' }, function(err, output) {
                            if(err) next(err);
                            else {
                                var points = 0;
                                var hours = 0;
                                var currentYear = new moment({years: now.year()-1, months: 3, date: 1});
                                var nextYear = new moment({years: now.year(), months: 3, date: 1});
                                var endTime = now.valueOf() < nextYear.valueOf() ? currentYear : nextYear;
                                var startTime = new moment({years: endTime.year()-1, months: 3, date: 1});

                                for(var i = 0; i < events.length; i++) {
                                    var item = events[i];
                                    var time = item.session.startTime ? new moment(item.session.startTime) : null;
                                    if(!time) time = item.records[0] && item.records[0].joinTime ? new moment(item.records[0].joinTime) : null;
                                    if(time && time.valueOf() >= startTime.valueOf() && time.valueOf() < endTime.valueOf()) {
                                        points += item.points;
                                    }
                                    if(time.year() == now.year())
                                        hours += item.hours;
                                }

                                user.points += points;
                                var curRank = rankDict[user.rank];
                                if(curRank && curRank.next) {
                                    if(user.points >= curRank.next.points_total) {
                                        user.rank = curRank.next._id;
                                    }
                                }

                                        user.save(function(err) {
                                            cb(err);
                                        });
                            }
                        });
                    }
                });

            }, function(err) {
                if(err)
                    cb(err);
                else
                    res.send("Success");
            });
        });
    });
    
});

module.exports = router;


