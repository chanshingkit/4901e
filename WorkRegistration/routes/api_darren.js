﻿var express = require('express');
var router = express.Router();
var models = require('../models.js');

var formidable = require('formidable');
var mkdirp = require('mkdirp');
var fs = require('fs');
var importExcel = require('../tasks/importExcel.js');

var uuid = require('node-uuid');

var dqs = function (q) {
    q = q? q : "DEFAULT_QUERY_STRING"; //Make sure you can't find anything.
}

router.use(function (req, res, next) {
    if (req.body && req.body.token) {
        models.users.validateUser(req.body.token, function (result) {
            if (result) {
                req.user = result
                next();
            } else {
                next();
            }
        });
    } else {
        next();
    }
});

router.post('/checkToken', function (req, res) {
    res.send({ status: !!req.user });
});

router.post('/check', function (req, res) {
    models.users.findOne({ email: req.body.email }, function (err, user) {
        if (err || user) {
            res.send({ status: false });
        } else {
            res.send({ status: true });
        }
    });
});

router.post('/register', function (req, res) {
    var user = new models.users(req.body);
    user.password = models.users.hash(req.body.pass || "");
    user.token = models.users.token();
    user.save(function (err) {
        if (err) {
            res.send({ status: false });
        } else {
            res.send({ status: true, token: user.token });
        }
    });
});

router.post('/signin', function (req, res) {
    models.users.loginUser(req.body.email, req.body.pass, function (err, user) {
        if (err) {
            res.send({ status: false });
        } else {
            user.token = models.users.token();
            user.save(function (err) {
                if (err) {
                    res.send({ status: false });
                } else {
                    res.send({ status: true, token: user.token });
                }
            });
        }
    });
});

router.get('/getEvent', function (req, res, next) {
    //Backdoor for dev - web console will use entirely POST messages
    models.events.find(req.query).exec(function (error, doc) {
        if (error) {
            console.log(err); next(err);
        } else {
            res.status(200).send(doc);
        }
    })
});

router.post('/getEvent', function (req, res, next) {
    models.events.find(req.body).exec(function (error, doc) {
        if (error) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    })
});

router.post('/addEvent', function (req, res, next) {
    var nevent = new models.events(req.body);
    nevent.save(function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(nevent);
        }
    });
});

router.post('/updateEvent', function (req, res, next) {
    models.events.findOneAndUpdate({ _id: req.body._id }, { $set: req.body }, { new: true }, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.post('/deleteEvent', function (req, res, next) {
    models.events.findOneAndRemove({ _id: req.body._id }, {}, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.get('/getUser', function (req, res, next) {
    //Backdoor for dev - web console will use entirely POST messages
    models.users.find(req.query).populate("rank").exec(function (error, doc) {
        if (error) {
            console.log(err); next(err);
        } else {
            res.status(200).send(doc);
        }
    })
});

router.post('/getUser', function (req, res, next) {
    models.users.find(req.body).populate("rank").exec(function (error, doc) {
        if (error) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    })
});

router.post('/addUser', function (req, res, next) {
    var nuser = new models.users(req.body);
    nuser.token = models.users.token();
    dqs(req.body.rank_name_eng);
    models.ranks.findOne({ name_eng: req.body.rank_name_eng }, function (err, frank) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            if (frank) {
                nuser.rank = frank._id;
            } else {
                nuser.rank = null;
            }
            nuser.save(function (err, doc) {
                if (err) {
                    console.log(err); res.status(500).send(err.toString());
                } else {
                    res.status(200).send(nuser);
                }
            });
        }
    });
});

router.post('/updateUser', function (req, res, next) {
    dqs(req.body.rank_name_eng);
    models.ranks.findOne({ name_eng: req.body.rank_name_eng }, function (err, frank) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            if (frank) {
                req.body.rank = frank._id;
            } else {
                req.body.rank = null;
            }
            models.users.findOneAndUpdate({ _id: req.body._id }, { $set: req.body }, { new: true }, function (err, doc) {
                if (err) {
                    console.log(err); res.status(500).send(err.toString());
                } else {
                    res.status(200).send(doc);
                }
            });
        }
    });
});

router.post('/deleteUser', function (req, res, next) {
    models.users.findOneAndRemove({ _id: req.body._id }, {}, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.get('/getRank', function (req, res, next) {
    //Backdoor for dev - web console will use entirely POST messages
    models.ranks.find(req.query).populate("points_subtotal.sub_level").populate("prereq").exec(function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.post('/getRank', function (req, res, next) {
    models.ranks.find(req.body).populate("points_subtotal.sub_level").populate("prereq").exec(function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.post('/addRank', function (req, res, next) {
    dqs(req.body.prereq_name);
    dqs(req.body.points_subtotal_sub_level_name);
    var nobj = new models.ranks(req.body);
    nobj.token = models.ranks.token();
    
    models.eventRanks.findOne({ name_eng: req.body.points_subtotal_sub_level_name }, function (err, ferank) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            if (ferank) {
                nobj.points_subtotal.sub_level = ferank._id;
            } else {
                nobj.points_subtotal.sub_level = null;
            }
            models.ranks.findOne({ name_eng: req.body.prereq_name }, function (err, frank) {
                if (err) {
                    console.log(err); res.status(500).send(err.toString());
                } else {
                    if (frank) {
                        nobj.prereq = frank._id;
                    } else {
                        nobj.prereq = null;
                    }
                    nobj.save(function (err, doc) {
                        if (err) {
                            console.log(err); res.status(500).send(err.toString());
                        } else {
                            res.status(200).send(nobj);
                        }
                    });
                }
            });
        }
    });
});

router.post('/updateRank', function (req, res, next) {
    dqs(req.body.prereq_name);
    dqs(req.body.points_subtotal_sub_level_name);
    
    models.eventRanks.findOne({ name_eng: req.body.points_subtotal_sub_level_name }, function (err, ferank) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            if (ferank) {
                req.body.points_subtotal.sub_level = ferank._id;
            } else {
                req.body.points_subtotal.sub_level = null;
            }
            models.ranks.findOne({ name_eng: req.body.prereq_name }, function (err, frank) {
                if (err) {
                    console.log(err); res.status(500).send(err.toString());
                } else {
                    if (frank) {
                        req.body.prereq = frank._id;
                    } else {
                        req.body.prereq = null;
                    }
                    models.ranks.findOneAndUpdate({ _id: req.body._id }, { $set: req.body }, { new: true }, function (err, doc) {
                        if (err) {
                            console.log(err); res.status(500).send(err.toString());
                        } else {
                            res.status(200).send(doc);
                        }
                    });
                }
            });
        }
    });
});

router.post('/deleteRank', function (req, res, next) {
    models.ranks.findOneAndRemove({ _id: req.body._id }, {}, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.get('/getEventRecord', function (req, res, next) {
    //Backdoor for dev - web console will use entirely POST messages
    models.eventRecords.find(req.query).populate("session").populate("user").populate("eventRank").exec(function (err, eventRecords) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(eventRecords);
        }
    });
});

router.post('/getEventRecord', function (req, res, next) {
    models.eventRecords.find(req.body).populate("session").populate("user").populate("eventRank").exec(function (err, eventRecords) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(eventRecords);
        }
    });
});

router.post('/addEventRecord', function (req, res, next) {
    //Promise is desired
    dqs(req.body.event_name_eng);
    dqs(req.body.user_name_eng);
    dqs(req.body.eventRank_name_eng);
    console.log(req.body.points);
    var neventbody = new models.eventRecords(req.body);
    //neventbody.token = models.eventRecords.token();
    models.events.findOne({ name: req.body.event_name_eng }, function (err, fevent) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            if (fevent) {
                neventbody.event = fevent._id;
            } else {
                neventbody.event = null;
            }
            models.users.findOne({ userName: req.body.user_name_eng }, function (err, fuser) {
                if (err) {
                    console.log(err); res.status(500).send(err.toString());
                } else {
                    if (fuser) {
                        neventbody.user = fuser._id;
                    } else {
                        neventbody.user = null;
                    }
                    models.eventRanks.findOne({ name_eng: req.body.eventRank_name_eng }, function (err, ferank) {
                        if (err) {
                            console.log(err); res.status(500).send(err.toString());
                        } else {
                            if (ferank) {
                                neventbody.eventRank = ferank._id;
                            } else {
                                neventbody.eventRank = null;
                            }
                            neventbody.save(function (err, event) {
                                if (err) {
                                    console.log(err); res.status(500).send(err.toString());
                                } else {
                                    res.status(200).send(neventbody);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});

router.post('/updateEventRecord', function (req, res, next) {
    //Promise is desired
    dqs(req.body.event_name_eng);
    dqs(req.body.user_name_eng);
    dqs(req.body.eventRank_name_eng);
    console.log(req.body.eventRank_name_eng);
    
    models.events.findOne({ name: req.body.event_name_eng }, function (err, fevent) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            if (fevent) {
                req.body.event = fevent._id;
            } else {
                req.body.event = null;
            }
            models.users.findOne({ userName: req.body.user_name_eng }, function (err, fuser) {
                if (err) {
                    console.log(err); res.status(500).send(err.toString());
                } else {
                    if (fuser) {
                        req.body.user = fuser._id;
                    } else {
                        req.body.user = null;
                    }
                    models.eventRanks.findOne({ name_eng: req.body.eventRank_name_eng }, function (err, ferank) {
                        if (err) {
                            console.log(err); res.status(500).send(err.toString());
                        } else {
                            if (ferank) {
                                console.log("here");
                                req.body.eventRank = ferank._id;
                            } else {
                                console.log("no");
                                req.body.eventRank = null;
                            }
                            models.eventRecords.findOneAndUpdate({ _id: req.body._id }, { $set: req.body }, { new: true }, function (err, doc) {
                                if (err) {
                                    console.log(err); res.status(500).send(err.toString());
                                } else {
                                    res.status(200).send(doc);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});

router.post('/deleteEventRecord', function (req, res, next) {
    models.eventRecords.findOneAndRemove({ _id: req.body._id }, {}, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.get('/getEventRank', function (req, res, next) {
    //Backdoor for dev - web console will use entirely POST messages
    models.eventRanks.find(req.query).exec(function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.post('/getEventRank', function (req, res, next) {
    models.eventRanks.find(req.body).exec(function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.post('/addEventRank', function (req, res, next) {
    var neventrank = new models.eventRanks(req.body);
    neventrank.save(function (err, event) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(event);
        }
    });
});

router.post('/updateEventRank', function (req, res, next) {
    models.eventRanks.findOneAndUpdate({ _id: req.body._id }, { $set: req.body }, { new: true }, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.post('/deleteEventRank', function (req, res, next) {
    models.eventRanks.findOneAndRemove({ _id: req.body._id }, {}, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.get('/getEventSession', function (req, res, next) {
    models.eventSessions.find(req.query).populate("event").exec(function (err, eventRecords) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(eventRecords);
        }
    });
});

router.post('/getEventSession', function (req, res, next) {
    models.eventSessions.find(req.body).populate("event").exec(function (err, eventRecords) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(eventRecords);
        }
    });
});

router.post('/addEventSession', function (req, res, next) {
    if (req.body.event_id) {
        req.body.event = req.body.event_id;
    }
    req.body.checkInToken = uuid.v4();
    req.body.checkOutToken = uuid.v4();
    var neventsession = new models.eventSessions(req.body);
    neventsession.save(function (err, event) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(event);
        }
    });
});

router.post('/updateEventSession', function (req, res, next) {
    if (req.body.event_id) {
        req.body.event = req.body.event_id;
    }
    models.eventSessions.findOneAndUpdate({ _id: req.body._id }, { $set: req.body }, { new: true }, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.post('/deleteEventSession', function (req, res, next) {
    models.eventSessions.findOneAndRemove({ _id: req.body._id }, {}, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

var UploadDoc = function (req, res, next) {
    //data: java.io.File
    //logger.log('debug', 'start uploading');
    var form = new formidable.IncomingForm();
    
    var FileSubDir = '../uploads/';
    
    //var FileCateDir = resource_dir + '/' + siteId + '/' + category_name + '/';
    //var FileSubDir = FileCateDir + subcategory_name + '/';
    //var FileSubDir = ((subcategory_name) && (subcategory_name != null)) ? FileCateDir + subcategory_name + '/' : FileCateDir;
    var FilePath;
    var FileName;
    form.uploadDir = FileSubDir;
    form.keepExtensions = true;
    form.type = 'urlencoded';
    form.maxFieldsSize = '50mb';
    form.hash = 'sha1';
    form.onPart = function (part) {
        if (!part.filename) {
            form.handlePart(part);
        }
        var bufs = [];
        part.on('data', function (data) {
            bufs.push(data);
        });
        part.on('end', function () {
            var excelData = Buffer.concat(bufs);
            
            console.log(JSON.stringify(req.query));
            //In: excelData, save, cb 
            //Out: ErrorFromDB, Success, Log, File
            importExcel.doit(excelData, req.query.import_db, function (err, msg, logs, file) {
                var response = {
                    ErrorFromDB: err,
                    Success: msg,
                    Log: logs,
                    File: file ? file.toString('base64') : null
                }
                if (err) {
                    console.log(err);
                    console.log(err.stack);
                    res.status(500).send(response);
                } else {
                    res.send(response);
                }
            });
            

            //The function above throws uncaught exception and the process exited.
            //res.status(200).send("Upload complete. Now the Database is updated. See the grids to see effect.");
        });
        part.on('error', function (err) {
            console.log(err); res.status(500).send(err.toString());
        });
    }
    
    form.on('error', function (err) {
        console.log(err);
        req.resume();
    });
    
    form.parse(req, function (error, fields, files) {
        if (error) {
            console.log(error); res.status(500).send(error.toString());
        }
    });
};

router.post('/importExcel', UploadDoc);

router.get('/getStaff', function (req, res, next) {
    models.staffs.find(req.query).exec(function (err, staffs) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(staffs);
        }
    });
});

router.post('/getStaff', function (req, res, next) {
    models.staffs.find(req.body).exec(function (err, staffs) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(staffs);
        }
    });
});

router.post('/addStaff', function (req, res, next) {
    var staff = new models.staffs(req.body);
    staff.setPassword(req.body.hash);
    staff.save(function (err) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(staff);
        }
    });
});

router.post('/updateStaff', function (req, res, next) {
    //Old method is used. 
    models.staffs.find({ _id: dqs(req.body._id) }, function (err, docs) {
        if (err) { console.log(err); res.status(500).send(err.toString()); return; }
        else if (docs.length == 0) { return res.status(400).send('Request body not complete.'); }
        else {
            var user = docs[0];
            user.username = req.body.username;
            user.StaffID = req.body.StaffID;
            user.StaffName = req.body.StaffName;
            if (user.hash !== req.body.hash) { user.setPassword(req.body.hash); }
            user.save(function (err) {
                if (err) { logger.log('error', err); res.status(500).send(err); return; }
                else { return res.status(200).send(user); }
            });
        }
    });
});

router.post('/deleteStaff', function (req, res, next) {
    models.staffs.findOneAndRemove({ _id: req.body._id }, {}, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

router.get('/getEventReg', function (req, res, next) {
    models.eventRegs.find(req.query).populate("event").populate("user").exec(function (err, staffs) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(staffs);
        }
    });
});

router.post('/getEventReg', function (req, res, next) {
    models.eventRegs.find(req.body).populate("event").populate("user").exec(function (err, staffs) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(staffs);
        }
    });
});

router.post('/addEventReg', function (req, res, next) {
    dqs(req.body.user_UST_stid);
    dqs(req.body.event_EventID);
    var nobj = new models.eventRegs(req.body);
    
    models.users.findOne({ "UST.stid" : req.body.user_UST_stid }, function (err, fuser) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            if (fuser) {
                nobj.user = fuser._id;
            } else {
                nobj.user = null;
            }
            models.events.findOne({ EventID: req.body.event_EventID }, function (err, fevent) {
                if (err) {
                    console.log(err); res.status(500).send(err.toString());
                } else {
                    if (fevent) {
                        nobj.event = fevent._id;
                    } else {
                        nobj.event = null;
                    }
                    nobj.save(function (err, doc) {
                        if (err) {
                            console.log(err); res.status(500).send(err.toString());
                        } else {
                            res.status(200).send(nobj);
                        }
                    });
                }
            });
        }
    });
});

router.post('/updateEventReg', function (req, res, next) {
    dqs(req.body.user_UST_stid);
    dqs(req.body.event_EventID);
    
    models.events.findOne({ EventID: req.body.event_EventID }, function (err, fevent) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            if (fevent) {
                req.body.event = fevent._id;
            } else {
                req.body.event = null;
            }
            models.users.findOne({ "UST.stid": req.body.user_UST_stid }, function (err, fuser) {
                if (err) {
                    console.log(err); res.status(500).send(err.toString());
                } else {
                    if (fuser) {
                        req.body.user = fuser._id;
                    } else {
                        req.body.user = null;
                    }    
                    models.eventRegs.findOneAndUpdate({ _id: req.body._id }, { $set: req.body }, { new: true }, function (err, doc) {
                        if (err) {
                            console.log(err); res.status(500).send(err.toString());
                        } else {
                            res.status(200).send(doc);
                        }
                    });
                }
            });
        }
    });
});

router.post('/deleteEventReg', function (req, res, next) {
    models.eventRegs.findOneAndRemove({ _id: req.body._id }, {}, function (err, doc) {
        if (err) {
            console.log(err); res.status(500).send(err.toString());
        } else {
            res.status(200).send(doc);
        }
    });
});

module.exports = router;